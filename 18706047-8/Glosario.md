# Glosario
1. control de versiones (V):
 Se llama control de versiones a la gestión de los diversos cambios que  se realizan sobre los elementos de algún producto o una configuración del mismo. Una versión, revisión o edición de un producto, es el estado en el que se encuentra el mismo en un momento dado de su desarrollo o modificación
https://es.wikipedia.org/wiki/Programas_para_control_de_versiones
2. Control de versiones distribuidos(DVC):
En programación informática, el control de versiones distribuido permite a muchos desarrolladores de software trabajar en un proyecto común sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido (DVCS, por sus siglas en inglés).
https://es.wikipedia.org/wiki/Control_de_versiones_distribuido
3. Repositorio remoto y repositorio local:
Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. Puedes tener varios, cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas.
 Y repositorio local es el que se encuentra en el computador o servidor local.
 https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
 4. Copia de trabajo/Working copy:
  Una copia de trabajo es una versión del proyecto. Estos archivos se sacan de la base de datos comprimida en el directorio de Git, y se colocan en disco para que los puedas usar o modificar.
 https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
 5. Area de preparación: 
  El área de preparación es un sencillo archivo, generalmente contenido en la directorio de Git, que almacena información acerca de lo que va a ir en la próxima confirmación. A veces se le denomina índice, pero se está convirtiendo en estándar el referirse a ella como el área de preparación.
 https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
 6. Preparar cambios:
  Preparar cambios significa que has marcado un archivo modificado en su versión actual para que vaya en la próxima confirmación.
 https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
 7. Confirmar cambios:
  Confirmar cambios significa que los datos o archivos marcados o preparados, se almacenen de manera segura en la base de datos local. 
 https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
 8. Commit:
  Commit Firma los cambios hechos en el repositorio local para marcar versiones realizadas.
 https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git
 9. Clone:
  Clone obtiene una copia de un repositorio remoto y la almacena localmente en un computador o servidor personal.
 https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git
 10. pull:
  pull es el comando utilizado para obtener todos los archivos del repositorio remoto, compararlos con el repositorio local y descargar solo los que poseen diferencia de version o no existen en el repositorio local
 https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
 11. push:
  push es el comando de git que ordena subir todos los cambios confirmados en el repositorio local al repositorio remoto.
 https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git
 12. fetch:
  Descarga los objetos y referencia desde uno o mas repositorios.
 https://git-scm.com/docs/git-fetch
 13. merge:
  Se utiliza para fusionar ramas o branches.
 https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
 14. status:
  Muestra las diferencias entre el ultimo commit del repositorio remoto y el local.
 https://git-scm.com/docs/git-fetch
 15. log:
  Muestra un historial de los commits que se han hecho en el repositorio o proyecto.
 https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones
 16. checkout:
  Permite moverse entre las ramas de un proyecto, con la posibilidad de crear nuevas en caso de ser necesario.
 https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
 17. Rama/Branch:
  Lineas independientes de desarrollo, se utilizan para desarrollar funcionalidades o probar otras.
 https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
 18. Etiqueta/tag:
  Los tags son utilizados para marcar versiones o momentos historicos de la rama, asi obtenerla en cualquier momento.
 https://www.genbeta.com/desarrollo/tags-con-git



